let canvas
let vid;
let playing = true;
let h,w;
let giffi;
function preload(){
  vid = createVideo("media/main.mp4");
  // vid.setAttribute('webkit-playsinline', 'webkit-playsinline');
  // webview.allowsInlineMediaPlayback = true;
  // img_mob = loadImage("media/main.gif");
}

function windowResized(){
  resizeCanvas(window.innerWidth,window.innerHeight)
}
function setup() {
  w = window.innerWidth;
  h = (w * 9) /43;
  createCanvas(window.innerWidth, window.innerHeight);
  img_mob = createImg("media/main.gif")
  img_mob.size(w,h-1)
  img_mob.position(0,50)


  vid.size(w, h);
  vid.volume(0);
  vid.loop();
  vid.position(0,50)
  vid.hide(); 
  // hides the html video loader


}

function draw() {
  let img = vid.get();
  image(img, 0, 50); 
  // resizeCanvas(window.innerWidth,window.innerHeight)
  // let giff = createImage("media/main.gif");
  // image(giff, 0, 50, w, h)
  // let img2 = img_mob.get()
  // image(img2, 0, 50); 
  // img_mob.position(0,0);
  // img_mob.size(100,100)

}



function mousePressed() {
 if (playing) {
   vid.pause();
 }
  else {
    vid.play();
  }
  playing = !playing;
}

// let isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
// if (isSafari) { 
//   $('head').append('<link rel="stylesheet" type="text/css" href="safari.css">') 
// };
